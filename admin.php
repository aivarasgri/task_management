<?php include "connection.php"; ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <link rel="stylesheet" href="styles/styles.css">
    <script src="myscripts/myscripts.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<body>

<div class="logoff-container">    
    <a href="index.php"><button class="btn-logoff">ATSIJUNGTI</button></a>     
</div>

<div class="task-header">
    <h1>UŽDUOTYS</h1>
</div>    

<div class="form-container">      
    <div class="add-task">
        <form action="admin.php" method="post">
            <p>Užduoties pavadinimas</p>
            <input type="text" name="task_title"><br>
            <p>Užduoties aprašymas</p>
            <textarea name="task_description" id="" cols="30" rows="10"></textarea><br>
            <p>Įrašykite, kam priskiriama užduotis<br>(vardas, pavardė)</p>
            <input type="text" name="user_id"><br><br>
<!--            <p>Data</p>-->
<!--            <input name="date" type="text"><br><br>-->
            <input type="submit" name="submit" value="Pridėkite naują užduotį">   
        </form>
    </div>
</div>         

     
<?php
    
//pasirenkame duomenis is duomenų bazės ir juos atvaizduojame
    
$query = "SELECT * FROM tasks ORDER BY date";
$selectAllTasks = mysqli_query($connection, $query);  
    
while($row = mysqli_fetch_assoc($selectAllTasks)){
    
    if(!$selectAllTasks){
        die("FAILED " . mysqli_error($connection));
    }
    
$task_title = $row['task_title'];
$task_description = $row['task_description'];
$date = $row['date'];
$task_id = $row['task_id'];
$user_id = $row['user_id'];    
    
    
    echo '<div class="task-container">';
        echo "<h2>$task_title </h2>";
        echo "<p>Užduotis: $task_description </p>";
        echo "<p>Užtuotis priskirta: $user_id </p>";
        echo "<p>Data:  $date </p>";
            echo '<div class="task-buttons">';
                echo "<a href='admin.php?delete={$task_id}'>IŠTRINTI</a> ";
                echo "<a href='admin.php?edit={$task_id}' >REDAGUOTI</a>";
            echo '</div>';
    echo "</div> ";
    
    } 

?>   


<?php
   
//Ištriname duomenis   

if(isset($_GET['delete'])){

   $the_task_id = $_GET['delete'];
    
    $query = "DELETE FROM tasks WHERE task_id = {$the_task_id}";
    $delete_query = mysqli_query($connection,$query);
    if(!$delete_query){
        echo "wasnt deleted";
    }else {
        echo "deleted";
    }
    header("Location: admin.php");
    exit;
    
}                 
                 
?>   
    
<!--ikeliame duomenis į duomenų bazę-->    
    
<?php 
    
if(isset($_POST['submit'])){
    
    if(isset($_POST['task_title']) && isset($_POST['task_description']) && isset($_POST['user_id'])){
    
$task_title = $row['task_title'];
$task_description = $row['task_description'];
$user_id = $row['user_id'];         
$date = date('d-m-y');    
    
$sql = "INSERT INTO tasks (task_title, task_description, user_id, date) VALUES('" . $_POST["task_title"] . "', '" . $_POST["task_description"] . "', '" . $_POST["user_id"] . "', now()) ";
        
//patikriname ar sukūrėme naują užduotį        
    
    if (mysqli_query($connection, $sql)) {

		    echo "New record created successfully";

		} else {
		    echo "Error: " . $sql . "<br>" . mysqli_error($connection);
		}
    }
// perkrauna tinklapį automatiškai is sustabdo programą    
    header("Location: admin.php");
    exit;
}
                                       
    
?>    

        
<!--išsirenkame duomenis------>


<div class="task-header" id="">
    <h1>REDAGUOKITE</h1>
</div> 

<div class="update-container">      
    <div class="add-task">
        <form action="admin.php" method="post">
            <p>Užduoties pavadinimas</p>
            
            
<?php
             
            
if(isset($_GET['edit'])){   
    $task_id = $_GET['edit'];
            
$query = "SELECT * FROM tasks WHERE task_id = $task_id";
$selectAllTasksid = mysqli_query($connection, $query);  
    
while($row = mysqli_fetch_assoc($selectAllTasksid)){
    
//$task_id = $row['task_id'];    
$task_title = $row['task_title'];
$task_description = $row['task_description'];
//$date = $row['date'];

    
?>



<input value="<?php if(isset($task_title)){echo $task_title;}?>" type="text" name="task_title"><br>

<p>Užduoties aprašymas</p>

<textarea rows="4" cols="30" class="update-input" value="<?php if(isset($task_description)){echo $task_description;}?>" type="text" name="task_description"></textarea><br><br>

<?php }} ?>
   
<?php // redaguojame duomenis

if(isset($_POST['update_task'])){

$the_task_title = $_POST['task_title'];
$the_task_description = $_POST['task_description']; 
$the_task_id = $_POST['task_id']; // Comment: I added this!  
    
$query = "UPDATE tasks SET task_title = '{$the_task_title}', task_description = '{$the_task_description}' WHERE task_id = $the_task_id ";
$updateQuery = mysqli_query($connection,$query);
    
    
    if(!$updateQuery){
        die("Update failer" . mysqli_error($connection));
    }
    header("Location: admin.php");
    
            
}
            
?>
    
            
<!--            <p>Data</p>-->
<!--            <input name="date" type="text"><br><br>-->
           <input type="hidden" name="task_id" value="<?php echo $task_id; ?>">
            <input type="submit" name="update_task" value="Redaguoti">   
        </form>
    </div>
</div>           

     
</body>
</html>